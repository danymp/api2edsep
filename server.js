//version inicial
var requestjson = require('request-json');
var path = require('path');
var session = require('express-session');
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var express = require('express');
var nodemailer = require('nodemailer');
var cookieParser = require('cookie-parser');
  app = express(),
  port = process.env.PORT || 3000;
app.listen(port);
app.set('trust proxy',true);
app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
// CORS-E ABAJO
app.use(cookieParser());
app.use(session({
    secret: 'connect.sid',
    cookie: {
        path: '/',
        domain: 'ec2-18-218-184-153.us-east-2.compute.amazonaws.com',
        maxAge: 600000 //1000 * 60 * 24 // 24 hours
    }
}));
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    next();
});
// CORS-E ARRIBA

var transporter = nodemailer.createTransport({
 service: 'gmail',
 auth: {
        user: 'jdanielmontes024@gmail.com',
        pass: 'Piloto91'
    }
});
console.log('SERVIDOR INICIADO EN PUERTO: ' + port);

// LOGIN
var URR_BASE_MLAB = "https://api.mlab.com/api/1/databases/jmontes/collections";
var apikey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var URL_MLAB_USUARIOS;
var BCRYPT_SALT_ROUNDS = 10;

app.post('/v1/login',function(req,res){
  var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
  var origin = req.headers.origin;
  var agente = req.get('user-agent');
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  var user = req.headers.user;
  var password = req.headers.password;
  if(user == null || password == null){
    res.status(401).send("Credenciales no validas");
  }else{
      var query = 'q={"usuario":"'+user+'"}';
      URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/usuarios?" + apikey + "&" + query);
      URL_MLAB_USUARIOS.get('',function(err,resM,body){
        if(!err){
          if(body.length == 0){
            log_canal(user,"Login","Fallido - usuario no existe",agente,origin,ip);
            req.session.destroy();
            res.status(403).send("Datos incorrectos.");
          }else if(body.length == 1){
            let password_hash = body[0].password;
            let intentos = body[0].access_attempts;
            if(intentos >= 3){
              log_canal(user,"Login","Fallido - Usuario bloqueado",agente,origin,ip);
              res.status(403).send("Usuario bloqueado.");
            }else{
              if(bcrypt.compareSync(password, password_hash)) {
                req.session.loggedin = true;
                var hour = 600000 ;   // 300'000 = 5 MIn      ------- 600'000 = 10 min
                req.session.cookie.expires = new Date(Date.now() + hour);
                req.session.cookie.maxAge = hour;
                //
                var datosC = {};
                var llave = 'cuentas';
                datosC[llave] = [] ;
                var temporal;
                for (var i = 0; i < body[0].tarjetas.length; i++) {
                  temporal = {
                    banco:body[0].tarjetas[i].banco,
                    saldo:body[0].tarjetas[i].saldo,
                    numero:body[0].tarjetas[i].numero
                  };
                  datosC[llave].push(temporal);
                }
                //
                var datosUcliente = {
                  "nombre":body[0].nombre,
                  "apellidos":body[0].apellidos,
                  "email":body[0].email,
                  "usuario":body[0].usuario,
                  "perfil":body[0].perfil,
                  "foto":body[0].foto,
                  "area":body[0].area,
                  "comedor":body[0].comedor,
                  "tarjetas":datosC.cuentas
                };
                // DATOS
                req.session.datos = datosUcliente;
                log_canal(user,"Login","Exitoso",agente,origin,ip);
                res.status(200).json(datosUcliente);
              } else {
                log_canal(user,"Login","Fallido - Password Incorrecto",agente,origin,ip);
                let query2 = 'q={"usuario":"'+user+'"}&m=false';
                intentos++;
                let datosUcliente = {
                  "$set":{
                    "access_attempts":intentos
                  }
                };
                URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/usuarios?" + apikey + "&" + query2);
                URL_MLAB_USUARIOS.put('',datosUcliente,function(err3,resM2,body){});
                req.session.destroy();
                res.status(403).send("Datos incorrectos.");
              }
            }
          }else{
            log_canal(user,"Login","Fallido - multples usuarios",agente,origin,ip);
            req.session.destroy();
            res.status(403).send("Datos incorrectos.");
          }
        }else{
          log_canal("Anonimo","Login","Fallido - " + user,agente,origin,ip);
          req.session.destroy();
          res.status(500).send("Internal Error. Contacta al administrador.");
        }
      });
  }
});

app.get("/v1/logout",function(req,res){
  var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
  var origin = req.headers.origin;
  var user = req.session.datos.usuario;
  var agente = req.get('user-agent');
  log_canal(user,"Logout","Exitoso",agente,origin,ip);
  req.session.destroy();
  res.status(200).send("Usuario salio correctamente");
});

// DESBLOQUEO - ENVIO DE CORREO CON CLAVE
app.post('/v1/token',function(req,res){
  var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
  var origin = req.headers.origin;
  var agente = req.get('user-agent');
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  var user = req.headers.user;
  if(user == null){
    res.status(401).send("Credenciales no validas.");
  }else{
      var query = 'q={"usuario":"'+user+'"}';
      URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/usuarios?" + apikey + "&" + query);
      URL_MLAB_USUARIOS.get('',function(err,resM,body){
        if(!err){
          if(body.length == 0){
            log_canal("Anonimo","Genera Token","Fallido -usuario no existe " + user,agente,origin,ip);
            res.status(500).send("Internal Error. Contacta al administrador.");
          }else if(body.length == 1){
            let email = body[0].email;
            let usuario = body[0].usuario;
            let aleatorio = "" + Math.round(Math.random()*999999);
            var datosUcliente = {
              "usuario":body[0].usuario,
              "codigo":aleatorio
            };
            let accion_post = 0;
            var query2 = 'q={"usuario":"'+user+'"}';
            URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/desbloqueo?" + apikey + "&" + query2);
            URL_MLAB_USUARIOS.get('',function(err,resM,body){
              if(!err){
                URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/desbloqueo?" + apikey);
                if(body.length == 0){
                  URL_MLAB_USUARIOS.post('',datosUcliente,function(err1,resM1,body){
                  if (err1) {
                    log_canal(user,"Genera Token","Fallido - Tecnico",agente,origin,ip);
                    res.status(500).send("Internal Error.");
                  }else{
                    var mailOptions = {
                      from: 'jdanielmontes024@gmail.com',
                      to: email,
                      subject: 'TECHU UNIVERSITY - DESBLOQUEO CUENTA',
                      html: '<p>Introduce la siguiente clave en tu proceso de desbloqueo: '+ aleatorio + '</p>'
                    };
                    transporter.sendMail(mailOptions, function (err, info) {
                      if(err){
                        log_canal(user,"Genera Token","Fallido - Envio Email",agente,origin,ip);
                        res.status(500).send("Internal Error. Contacta al administrador.");
                      }else{
                        log_canal(user,"Genera Token","Exitoso",agente,origin,ip);
                        res.status(200).send("Revisa tu correo para continuar con el desbloqueo.");
                      }
                    });
                  }
                });
                }else if(body.length == 1){
                  URL_MLAB_USUARIOS.put('',datosUcliente,function(err1,resM1,body){
                  if (err1) {
                    log_canal(user,"Genera Token","Fallido - Tecnico",agente,origin,ip);
                    res.status(500).send("Internal Error. Contacta al administrador.");
                  }else{
                    var mailOptions = {
                      from: 'jdanielmontes024@gmail.com',
                      to: email,
                      subject: 'TECHU UNIVERSITY - DESBLOQUEO CUENTA',
                      html: '<p>Introduce la siguiente clave en tu proceso de desbloqueo: '+ aleatorio + '</p>'
                    };
                    transporter.sendMail(mailOptions, function (err, info) {
                      if(err){
                        log_canal(user,"Genera Token","Fallido - Tecnico",agente,origin,ip);
                        res.status(500).send("Internal Error. Contacta al administrador.");
                      }else{
                        log_canal(user,"Genera Token","Exitoso",agente,origin,ip);
                        res.status(200).send("Revisa tu correo para continuar con el desbloqueo.");
                      }
                    });
                  }
                });
              }else{
                log_canal(user,"Genera Token","Fallido - Multiple tokens",agente,origin,ip);
                res.status(500).send("Internal Error. Contacta al administrador.");
              }
            }else{
              log_canal(user,"Genera Token","Fallido - Tecnico",agente,origin,ip);
              res.status(500).send("Internal Error. Contacta al administrador.");
            }
          });
        }else{
          log_canal("Anonimo","Genera Token","Fallido -usuario Multiple " + user,agente,origin,ip);
          res.status(500).send("Internal Error. Contacta al administrador.");
        }
      }else{
        log_canal("Anonimo","Genera Token","Fallido - " + user,agente,origin,ip);
        res.status(500).send("Errro de comunicaciones. Intenta mas tarde");
      }
    });
  }
});

app.post('/v1/desbloqueo',function(req,res){
  var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
  var origin = req.headers.origin;
  var agente = req.get('user-agent');
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  var user = req.headers.user;
  var token = req.headers.token;
  var password = req.headers.password;
  if(user == null || token == null){
    res.status(500).send("Ingresa un usuario-token para desbloqueo");
  }else{
      var query = 'q={"usuario":"'+user+'"}';
      // Consulta si el cliente tiene un token
      URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/desbloqueo?" + apikey + "&" + query);
      URL_MLAB_USUARIOS.get('',function(err,resM,body){
        if(!err){
          if(body.length == 0){
            log_canal("Anonimo","Desbloqueo","Fallido - No existe usuario " + user,agente,origin,ip);
            res.status(500).send("Internal Error. Contacta al administrador.");
          }else if(body.length == 1){
            let documento = body[0]._id.$oid;
            let codigo = body[0].codigo;
            if(codigo != token){
              log_canal(user,"Desbloqueo","Token no valido",agente,origin,ip);
              res.status(500).send("Internal Error. Contacta al administrador.");
            }else{
              let query2 = 'q={"usuario":"'+user+'"}&m=false';
              let body2 = '{$set:{"password":"'+password+'"}}';
              let hash = bcrypt.hashSync(password, BCRYPT_SALT_ROUNDS);
              let datosUcliente = {
                "$set":{
                  "password":hash,
                  "access_attempts":0
                }
              };
              URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/usuarios?" + apikey + "&" + query2);
              URL_MLAB_USUARIOS.put('',datosUcliente,function(err3,resM2,body){
                if (err3) {
                  log_canal(user,"Desbloqueo","Fallido - al guardar",agente,origin,ip);
                  res.status(500).send("Internal Error. Contacta al administrador.");
                }else{
                  URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/desbloqueo/"+ documento + "?" + apikey);
                  URL_MLAB_USUARIOS.delete('',function(err3,resM2,body){
                      if(err3){
                        log_canal(user,"Desbloqueo","falldo - eliminar token",agente,origin,ip);
                      }
                 });
                 log_canal(user,"Desbloqueo","Exitoso",agente,origin,ip);
                 res.status(200).send("Tu clave de acceso a sido actualizada");
                }
              });
            }
        }else{
          log_canal(user,"Desbloqueo","Fallido - Multiple usuarios - " + user,agente,origin,ip);
          res.status(500).send("Internal Error. Contacta al administrador.");
        }
      }else{
        log_canal("Anonimo","Desbloqueo","Fallido",agente,origin,ip);
        res.status(500).send("Internal Error. Contacta al administrador.");
      }
    });
  }
});


app.get('/v1/cuentas',function(req,res){
  if(req.session.loggedin){
    var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
    var origin = req.headers.origin;
    var agente = req.get('user-agent');
    var user = req.session.datos.usuario;
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var query = 'q={"usuario":"'+user+'"}';
    URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/usuarios?" + apikey + "&" + query);
    URL_MLAB_USUARIOS.get('',function(err,resM,body){
      if(!err){
        var datosC = {};
        var llave = 'cuentas';
        datosC[llave] = [] ;
        var temporal;
        for (var i = 0; i < body[0].tarjetas.length; i++) {
          temporal = {
            banco:body[0].tarjetas[i].banco,
            saldo:body[0].tarjetas[i].saldo,
            numero:body[0].tarjetas[i].numero
          };
          datosC[llave].push(temporal);
        }
        log_canal(user,"Consulta cuentas/tarjetas","Exitoso",agente,origin,ip);
        res.status(200).json(datosC.cuentas);
      }else{
        log_canal(user,"Consulta cuentas/tarjetas","Fallido - Tecnico",agente,origin,ip);
        req.session.destroy();
        res.status(500).send("Internal Error. Contacta al administrador.");
      }
    });
  }else{
    res.status(500).send("Login");
  }
});


app.get('/v1/comedor',function(req,res){
  if(req.session.loggedin){
    var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
    var origin = req.headers.origin;
    var agente = req.get('user-agent');
    var user = req.session.datos.usuario;
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var query = 'q={"usuario":"'+user+'"}';
    URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/usuarios?" + apikey + "&" + query);
    URL_MLAB_USUARIOS.get('',function(err,resM,body){
      if(!err){
        var datosUcliente = {
          "comedor":body[0].comedor
        };
        log_canal(user,"Consulta saldo comedor","Exitoso",agente,origin,ip);
        res.status(200).json(datosUcliente);
      }else{
        log_canal(user,"Consulta saldo comedor","Fallido - Tecnico",agente,origin,ip);
        req.session.destroy();
        res.status(500).send("Internal Error. Contacta al administrador.");
      }
    });
  }else{
    res.status(500).send("Login");
  }
});

app.get('/v1/dashboard',function(req,res){
  if(req.session.loggedin){
    var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
    var origin = req.headers.origin;
    var agente = req.get('user-agent');
    var user = req.session.datos.usuario;
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var query = 'q={"usuario":"'+user+'"}';
    URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/usuarios?" + apikey + "&" + query);
    URL_MLAB_USUARIOS.get('',function(err,resM,body){
      if(!err){
        var datosC = {};
        var llave = 'cuentas';
        datosC[llave] = [] ;
        var temporal;
        for (var i = 0; i < body[0].tarjetas.length; i++) {
          temporal = {
            banco:body[0].tarjetas[i].banco,
            saldo:body[0].tarjetas[i].saldo,
            numero:body[0].tarjetas[i].numero
          };
          datosC[llave].push(temporal);
        }
        var datosUcliente = {
          "nombre":body[0].nombre,
          "apellidos":body[0].apellidos,
          "email":body[0].email,
          "usuario":body[0].usuario,
          "perfil":body[0].perfil,
          "foto":body[0].foto,
          "area":body[0].area,
          "comedor":body[0].comedor,
          "tarjetas":datosC.cuentas
        };
        log_canal(user,"Consulta Dashboard","Exitoso",agente,origin,ip);
        res.status(200).json(datosUcliente);
      }else{
        log_canal(user,"Consulta Dashboard","Fallido - Tecnico",agente,origin,ip);
        req.session.destroy();
        res.status(500).send("Internal Error. Contacta al administrador.");
      }
    });
  }else{
    res.status(500).send("Login");
  }
});

app.post('/v1/recharge',function(req,res){
  if(req.session.loggedin){
    var ip = "";
    var origin = "";
    var agente = "";
    var tarjeta = "";
    var monto = "";
    var fecha_front = "";
    var cvv2_front = "";
    try {
      ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
      origin = req.headers.origin;
      agente = req.get('user-agent');
      tarjeta = req.headers.tarjeta;
      monto = req.headers.monto;
      fecha_front = req.headers.fecha;
      cvv2_front = req.headers.cvv2;
    }
    catch (e) {
     console.log("Fallo al obtener informacion de header");
    }
    var continuar = false;
    if(tarjeta == null || tarjeta == ""){
      res.status(500).send("Ingresa una tarjeta");
    }else{
      if(fecha_front == null || fecha_front == ""){
        res.status(500).send("Ingresa una fecha valida");
      }else{
        if(cvv2_front == null || cvv2_front == ""){
          res.status(500).send("Ingresa el cvv2");
        }else{
          if (monto == null || monto <=0 ){
            res.status(500).send("Ingresa un monto valido");
          }else{
            continuar = true;
          }
        }
      }
    }
    if(continuar){
    var user = req.session.datos.usuario;
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var query = 'q={"usuario":"'+user+'"}';
    URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/usuarios?" + apikey + "&" + query);
    URL_MLAB_USUARIOS.get('',function(err,resM,body){
      if(!err){
        var datosUcliente = {
          "tarjetas":body[0].tarjetas,
          "comedor":body[0].comedor
        };
        var saldo_comedor = parseFloat(datosUcliente.comedor.saldo);
        monto = parseFloat(monto);
        var saldo_tarjeta;
        var banco;
        var nuevo_saldo_tarjeta;
        var nuevo_saldo_comedor;
        var fecha_tarjeta;
        var cvv2_tarjeta;
        var tarjeta_cliente;
        for (var i = 0; i < datosUcliente.tarjetas.length; i++) {
          if (datosUcliente.tarjetas[i].numero == tarjeta) {
            saldo_tarjeta = parseFloat(datosUcliente.tarjetas[i].saldo);
            tarjeta_cliente = datosUcliente.tarjetas[i].numero;
            fecha_tarjeta = datosUcliente.tarjetas[i].fecha;
            cvv2_tarjeta = datosUcliente.tarjetas[i].cvv2;
            // VALIDACION DE DATOS abajo

            if(fecha_tarjeta == fecha_front){
              if(bcrypt.compareSync(cvv2_front, cvv2_tarjeta)) {
                banco = datosUcliente.tarjetas[i].banco;
                if(saldo_tarjeta < monto){
                  log_canal(user,"Recarga","Fallida - Dinero Insuficiente",agente,origin,ip);
                    res.status(500).send("No tienes suficiente dinero para recargar");
                }else{
                  nuevo_saldo_tarjeta = saldo_tarjeta - monto;
                  nuevo_saldo_comedor = saldo_comedor + monto;
                  datosUcliente.tarjetas[i].saldo = nuevo_saldo_tarjeta;
                  // ACTUALIZAR monto - ABAJO
                  let query2 = 'q={"usuario":"'+user+'"}&m=false';
                  let datosUclienteNuevos = {
                    "$set":{
                      "comedor":{
                        "saldo":nuevo_saldo_comedor
                      },
                      "tarjetas":datosUcliente.tarjetas
                    }
                  };
                  URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/usuarios?" + apikey + "&" + query2);
                  URL_MLAB_USUARIOS.put('',datosUclienteNuevos,function(err3,resM2,body){
                    if (err3) {
                      log_canal(user,"Recarga","Fallida - Tenico",agente,origin,ip);
                      res.status(500).send("Internal Error. Contacta al administrador.");
                    }else{
                      // guardar mov abajo
                      let usuario_1 = user;
                      let fecha_1 = new Date();
                      let movimiento_1 = "RECARGA DE SALDO";
                      let monto_1 = monto;
                      var datosUcliente_mov = {
                        "fecha":fecha_1,
                        "usuario":usuario_1,
                        "movimiento":movimiento_1,
                        "monto":monto_1,
                        "saldoComedor":nuevo_saldo_comedor
                      };

                      var dataS = {
                        "Saldo comedor":nuevo_saldo_comedor
                      };

                      URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/movimientos?" + apikey);
                        URL_MLAB_USUARIOS.post('',datosUcliente_mov,function(err1,resM1,body){
                        if (err1) {
                          res.status(500).send("Internal Error.");
                          log_canal(user,"Recarga","Exitoso - Fallo guardar movimiento",agente,origin,ip);
                        }else{
                          log_canal(user,"Recarga","Exitoso",agente,origin,ip);
                          res.status(200).send(dataS);
                        }
                      });
                      // guardar mov arriba
                    }
                  });
                  // ACTUALIZAR monto - ARRIBA
              }
              }else{
                log_canal(user,"Recarga","Fallido - CVV2 Incorrecto",agente,origin,ip);
                res.status(500).send("Datos incorrectos.");
              }
            }else{
              log_canal(user,"Recarga","Fallido - Fecha Incorrecta",agente,origin,ip);
              res.status(500).send("Datos incorrectos.");
            }
            // VALIDACION DE DATOS arriba
        }
      }// TERMINA FOR -  BARRIDO DE TARJETAS
        if(tarjeta_cliente == null || tarjeta_cliente == ""){
          log_canal(user,"Recarga","Fallido - Cuenta no existente",agente,origin,ip);
          res.status(500).send("Internal Error. Contacta al administrador.");
        }
      }else{
        log_canal(user,"Recarga","Fallido - Tecnico",agente,origin,ip);
        req.session.destroy();
        res.status(500).send("Internal Error. Contacta al administrador.");
      }
    });
  }else{

  }
  }else{
    res.status(500).send("Login");
  }
});


app.get('/v1/movements',function(req,res){
  if(req.session.loggedin){
    var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
    var origin = req.headers.origin;
    var agente = req.get('user-agent');
    var user = req.session.datos.usuario;
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var query = 'q={"usuario":"'+user+'"}';
    URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/movimientos?" + apikey + "&" + query);
    URL_MLAB_USUARIOS.get('',function(err,resM,body){
      if(!err){
        var datos = [];
        for (var i = 0; i < body.length; i++) {
          var datosUcliente = {
            "fecha":body[i].fecha,
            "movimiento":body[i].movimiento,
            "monto":body[i].monto,
            "saldo":body[i].saldoComedor
          };
          datos.push(datosUcliente);
        }
        log_canal(user,"Consulta de movimiento","Exitosa",agente,origin,ip);
        res.status(200).json(datos);
      }else{
        log_canal(user,"Consulta de movimiento","Fallida",agente,origin,ip);
        req.session.destroy();
        res.status(500).send("Internal Error. Contacta al administrador.");
      }
    });
  }else{
    res.status(500).send("Login");
  }
});

  function formatAMPM(date) {
    let fecha23 = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
    fecha23 = fecha23.substring(0, 10);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return fecha23 + " - " + strTime;
  }

var log_canal = function(usuario,operacion,descripcion,agente,origen,ip){
  let fecha = formatAMPM(new Date);
  console.log("Usuario: " + usuario +" - Operacion: " + operacion + " - Fecha: " + fecha);
  if(origen == null || origen == ""){
    origen = "Request GET";
  }
  var datosUcliente_mov = {
    "fechaX":fecha.substring(0, 10),
    "fecha":fecha,
    "usuario":usuario,
    "operacion":operacion,
    "resultado":descripcion,
    "navegador":agente,
    "app":origen,
    "ip":ip
  };
  URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/log?" + apikey);
    URL_MLAB_USUARIOS.post('',datosUcliente_mov,function(err1,resM1,body){});
}

app.get('/v1/bitacora',function(req,res){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var admin = req.headers.admin;
    var permisos = true;

    var operacionb = req.query.operacion;
    var query = "";
    if(operacionb != null && operacionb !=""){
      query = '&q={"operacion":"'+operacionb+'"}';
    }
    if(permisos){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/log?" + apikey   + query);
  URL_MLAB_USUARIOS.get('',function(err,resM,body){
    if(!err){
      var datos = [];
      for (var i = 0; i < body.length; i++) {
        var datosUcliente = {
          "fecha":body[i].fecha,
          "usuario":body[i].usuario,
          "operacion":body[i].operacion,
          "resultado":body[i].resultado,
          "navegador":body[i].navegador,
          "app":body[i].app,
          "ip":body[i].ip
        };
        datos.push(datosUcliente);
      }
      res.status(200).json(datos);
    }else{
      res.status(500).send("Internal Error. Consulta bitacora general.");
    }
  });
}else{
  res.status(500).send("Internal Error. Consulta bitacora general.");
}
});

app.get('/v1/bitacora/:id',function(req,res){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var admin = req.headers.admin;
    var permisos = true;
    if(permisos){
      var user = req.params.id;
      var operacionb = req.query.operacion;
      var fechab = req.query.fecha;
      var query = "";
      if(operacionb != null && operacionb !=""){
        query = 'q={"usuario":"'+user+'","operacion":"'+operacionb+'"}';
        if(fechab != null && fechab !=""){
          query = 'q={"usuario":"'+user+'","operacion":"'+operacionb+'","fechaX":"'+fechab+'"}';
        }
      }else{
        query = 'q={"usuario":"'+user+'"}';
        if(fechab != null && fechab !=""){
          query = 'q={"usuario":"'+user+'","fechaX":"'+fechab+'"}';
        }
      }
      URL_MLAB_USUARIOS = requestjson.createClient(URR_BASE_MLAB + "/log?" + apikey  + "&" + query);
      URL_MLAB_USUARIOS.get('',function(err,resM,body){
        if(!err){
          var datos = [];
          var datos2 = [];
          var Login = 0;
          var Consulta_Dashboard = 0;
          var Consulta_cuentas_tarjetas = 0;
          var Consulta_saldo_comedor = 0;
          var Consulta_de_movimiento = 0;
          var Recarga = 0;
          var Genera_Token = 0;
          var Desbloqueo = 0;
          for (var i = 0; i < body.length; i++) {
            if(body[i].operacion == "Login"){
              Login++;
            }
            if(body[i].operacion == "Consulta Dashboard"){
              Consulta_Dashboard++;
            }
            if(body[i].operacion == "Consulta cuentas/tarjetas"){
              Consulta_cuentas_tarjetas++;
            }
            if(body[i].operacion == "Consulta saldo comedor"){
              Consulta_saldo_comedor++;
            }
            if(body[i].operacion == "Consulta de movimiento"){
              Consulta_de_movimiento++;
            }
            if(body[i].operacion == "Recarga"){
              Recarga++;
            }
            if(body[i].operacion == "Genera Token"){
              Genera_Token++;
            }
            if(body[i].operacion == "Desbloqueo"){
              Desbloqueo++;
            }
            var datosUcliente = {
              "fecha":body[i].fecha,
              "usuario":body[i].usuario,
              "operacion":body[i].operacion,
              "resultado":body[i].resultado,
              "navegador":body[i].navegador,
              "app":body[i].app,
              "ip":body[i].ip
            };
            datos.push(datosUcliente);
          }
          var numeroOperaciones = {
            "login":Login,
            "Consulta_Dashboard":Consulta_Dashboard,
            "Consulta_cuentas_tarjetas":Consulta_cuentas_tarjetas,
            "Consulta_saldo_comedor":Consulta_saldo_comedor,
            "Consulta_de_movimiento":Consulta_de_movimiento,
            "Recarga":Recarga,
            "Genera_Token":Genera_Token,
            "Desbloqueo":Desbloqueo
          };
          datos2.push(datos);
          datos2.push(numeroOperaciones);
          res.status(200).json(datos2);
        }else{
          res.status(500).send("Internal Error. Consulta bitacora general.");
        }
      });
    }else{
      res.status(500).send("Internal Error. Consulta bitacora general.");
    }
});
